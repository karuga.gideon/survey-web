import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import SignIn from "./components/pages/SignIn";
import SignUp from "./components/pages/SignUp";
import HomePage from "./components/pages/HomePage";
import Dashboard from "./components/pages/app/Dashboard";
import Surveys from "./components/pages/app/Surveys";
import TopUp from "./components/pages/app/TopUp";
import Reports from "./components/pages/app/Reports";
import Profile from "./components/pages/app/Profile";
import About from "./components/pages/app/About";

// Forms
import CreateSurvey from "./components/pages/forms/create_survey";
import SurveyQuestions from "./components/pages/forms/survey_questions";

// View
import SurveyDetails from "./components/pages/view/SurveyDetails";

class App extends React.Component {
  render() {
    return (
      <div>
        <Router className="App">
          <React.Fragment>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/signin" component={SignIn} />
              <Route path="/signup" component={SignUp} />

              {/* Apps */}
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/surveys" component={Surveys} />
              <Route path="/topup" component={TopUp} />
              <Route path="/reports" component={Reports} />
              <Route path="/profile" component={Profile} />
              <Route path="/about" component={About} />

              {/* Forms  */}
              <Route path="/create-survey" component={CreateSurvey} />
              <Route
                path="/survey-questions/:surveyID"
                component={SurveyQuestions}
              />

              {/* Views */}
              <Route
                path="/survey-details/:surveyID"
                component={SurveyDetails}
              />

              {/* <Route path="*" component={NotFoundPage} /> */}
              <Route path="*" component={HomePage} />
            </Switch>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;

// Admin theme downloaded from
// https://demos.creative-tim.com/paper-dashboard-react/#/dashboard
