import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import damirBosnjak from "../../../assets/img/damir-bosnjak.jpg";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import dashboardRoutes from "../../../routes/dashboard.jsx";
import { Link } from "react-router-dom";
import { Segment, Icon, Table, Grid } from "semantic-ui-react";

import {
  isLoggedIn,
  getUserDetails,
  getSurveyDetails
} from "../../../util/AuthService";

import { API_BASE_URL, SURVEY_DETAILS } from "../../../constants";
import { Button, Form, Input, Label, TextArea } from "semantic-ui-react";

import axios from "axios";
import Loader from "../../shared/Loader";

class SurveyQuestions extends React.Component {
  constructor(props) {
    super(props);
    document.title = "SurveyQuestions";

    var surveyID = this.props.match.params.surveyID;
    console.log("surveyID >>> " + surveyID);

    var loggedIn = getUserDetails();
    console.log(isLoggedIn());
    console.log("loggedIn >>> " + loggedIn);
    console.log(loggedIn);

    var surveyDetails = getSurveyDetails();
    console.log("surveyDetails >>> " + surveyDetails);
    console.log(surveyDetails);

    this.handleFocus = this.handleFocus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeypress = this.handleKeypress.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleClick = this.handleClick.bind(this);

    this.helperspan = null; // is set via ref

    this.state = {
      currentcolor: ["#fff"],
      content_add: "Add +",
      width: 500,
      myItems: [],
      records: [],
      surveyDetails: {},
      surveyQuestions: [],
      isLoading: false,
      pageOfItems: [],
      surveyID: surveyID,
      question: "",
      organizationID: loggedIn.organization.id,
      organizationName: loggedIn.organization.name,
      contactPerson: loggedIn.organization.contact_person,
      phoneNumber: loggedIn.organization.phone_number,
      email: loggedIn.organization.email,
      description: loggedIn.organization.status_description,
      backgroundColor: "black",
      activeColor: "warning"
    };
    this.lastId = -1;
  }

  handleFocus(event) {
    this.setState({ content_add: "" });
  }

  handleChange(event) {
    const usr_input = event.target.value;
    this.setState({ content_add: usr_input, question: usr_input });
  }

  handleKeypress(event) {
    if (event.key == "Enter") {
      var newArray = this.state.myItems;
      var currentcontent = this.state.content_add.trim();
      if (!currentcontent) {
        return;
      }

      var currentWidth = this.helperspan.offsetWidth;
      newArray.push({
        content: currentcontent,
        id: ++this.lastId,
        itemWidth: currentWidth + 2
      });
      this.setState({
        myItems: newArray,
        content_add: ""
      });
    }
  }

  handleBlur(event) {
    this.setState({ content_add: "Add +" });
  }

  handleClick(event) {
    const idToRemove = Number(event.target.dataset["item"]);
    const newArray = this.state.myItems.filter(listitem => {
      return listitem.id !== idToRemove;
    });
    this.setState({ myItems: newArray });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.content_add != this.state.content_add) {
      console.log(
        "did update, content:",
        this.helperspan.textContent,
        "width",
        this.helperspan.offsetWidth
      );
      const helperWidth = this.helperspan.offsetWidth;
      this.setState({ width: Math.max(500, helperWidth + 1) });
    }
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {
    var loggedIn = getUserDetails();
    console.log("Getting user details ....?");
    console.log(loggedIn);

    this.setState({ isLoading: true });

    const { surveyID } = this.state;
    console.log("Getting Survey Details >>> " + surveyID);

    axios({
      url: `${API_BASE_URL}/get_survey/` + surveyID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log(response.data);
        // var surveyDetailsJSON = JSON.stringify(response.data);

        var json_data = JSON.parse(
          JSON.stringify(response.data.survey_questions)
        );
        console.log(json_data);

        this.setState({
          isLoading: false,
          surveyDetails: response.data,
          surveyQuestions: json_data
        });
        console.log(response.data);
        if (!response.data.error) {
          // alert("Your OTP Is : " + response.data.typeId);
          // alert("Survey details pulled Successfully.");
          localStorage.setItem(SURVEY_DETAILS);
          // this.props.history.push("/profile");
          // localStorage.setItem("Survey_Token", response.data.token);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }

  makeAddedList() {
    const elements = this.state.myItems.map((listitem, index) => (
      <li
        key={listitem.id}
        data-item={listitem.id}
        style={{
          backgroundColor: this.state.currentcolor[
            index % this.state.currentcolor.length
          ],
          width: listitem.itemWidth
        }}
      >
        {listitem.content}
        &nbsp;&nbsp;
        <a href="#" onClick={this.handleClick}>
          Remove
        </a>
        &nbsp;
        <a href="#">Edit</a>
      </li>
    ));
    return elements;
  }

  onAddSurveyQuestion = e => {
    this.setState({ isLoading: true });

    console.log("Adding survey question....");
    console.log("Survey ID >>> " + this.state.surveyID);
    console.log("Survey Question >>> " + this.state.question);

    var surveyPayLoad = {
      survey_id: parseInt(this.state.surveyID),
      question: this.state.question
    };
    console.log(surveyPayLoad);

    // Add question to survey
    axios({
      url: `${API_BASE_URL}/create_survey_question`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: surveyPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        console.log(response.data);
        if (!response.data.error) {
          this.props.history.push("/survey-questions/" + this.state.surveyID);
          getSurveyDetails();
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });

    // Pull latest survey details

    axios({
      url: `${API_BASE_URL}/get_survey/` + this.state.surveyID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log(response.data);
        // var surveyDetailsJSON = JSON.stringify(response.data);

        var json_data = JSON.parse(
          JSON.stringify(response.data.survey_questions)
        );
        console.log(json_data);

        this.setState({
          isLoading: false,
          surveyDetails: response.data,
          surveyQuestions: json_data
        });
        console.log(response.data);
        if (!response.data.error) {
          // alert("Your OTP Is : " + response.data.typeId);
          // alert("Survey details pulled Successfully.");
          localStorage.setItem(SURVEY_DETAILS);
          // this.props.history.push("/profile");
          // localStorage.setItem("Survey_Token", response.data.token);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {
    const { surveyDetails, surveyQuestions } = this.state;
    // console.log("Render surveyDetails >>> " + surveyDetails.id);
    console.log("Render surveyQuestions >>> " + surveyQuestions);

    let optionsButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Add Option
      </Form.Field>
    );

    let questionButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onAddSurveyQuestion}
      >
        Add Question
      </Form.Field>
    );

    let deleteButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-danger btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Delete
      </Form.Field>
    );

    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">
                  <div className="image">
                    <img src={damirBosnjak} alt="..." />
                  </div>
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      <h6> {surveyDetails.title} </h6>
                      Title : {surveyDetails.title} <br />
                      Target Number : {surveyDetails.target_number} <br />
                      Date Created: {surveyDetails.date_created} <br />
                      <br />
                      <h6>Description </h6>
                      {surveyDetails.description}
                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={8} xs={8}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Survey Questions</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Table
                      celled
                      selectable
                      responsive
                      style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                    >
                      <Table.Header>
                        <Table.Row style={{ color: "#51cbce" }}>
                          <Table.HeaderCell>Question</Table.HeaderCell>
                          <Table.HeaderCell>Actions</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>
                        {surveyQuestions.map(record => (
                          <Table.Row>
                            <Table.Cell>
                              {record.question} <br />
                              <br />
                              <Link to={`/survey-details/${record.id}`}>
                                {optionsButton}
                              </Link>
                            </Table.Cell>
                            <Table.Cell>
                              <Link to={`/survey-questions/${record.id}`}>
                                {deleteButton}
                              </Link>
                            </Table.Cell>
                          </Table.Row>
                        ))}
                      </Table.Body>
                    </Table>

                    <Label
                      style={{
                        fontSize: "0.8571em",
                        marginBottom: "5px",
                        color: "#9A9A9A"
                      }}
                    >
                      Add Questions
                    </Label>

                    <Form.Field className="form-control">
                      <TextArea
                        id="add"
                        type="text"
                        placeholder="Add Question"
                        className="form-control"
                        style={{
                          textDecoration: "none",
                          padding: "0px",
                          border: "0px"
                        }}
                        onChange={e =>
                          this.setState({ question: e.target.value })
                        }
                      />
                    </Form.Field>

                    <br />

                    <Row>
                      <div className="">
                        {/* <Button color="primary" round>
                            Update SurveyQuestions
                          </Button> */}

                        {questionButton}
                        <br />
                      </div>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default SurveyQuestions;
