import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import CardAuthor from "../../../components/admin/CardElements/CardAuthor.jsx";
import { Link } from "react-router-dom";

import damirBosnjak from "../../../assets/img/damir-bosnjak.jpg";

import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";

import dashboardRoutes from "../../../routes/dashboard.jsx";

import {
  logout,
  login,
  isLoggedIn,
  getUserDetails
} from "../../../util/AuthService";

var ps;

class About extends React.Component {
  constructor(props) {
    super(props);
    document.title = "About";

    var loggedIn = getUserDetails();
    console.log(isLoggedIn());
    console.log("loggedIn >>> " + loggedIn);
    console.log(loggedIn);

    this.state = {
      records: [],
      isLoading: false,
      pageOfItems: [],
      organizationID: loggedIn.organization.id,
      organizationName: loggedIn.organization.name,
      contactPerson: loggedIn.organization.contact_person,
      phoneNumber: loggedIn.organization.phone_number,
      email: loggedIn.organization.email,
      description: loggedIn.organization.status_description,
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };
  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };
  render() {
    const {
      organizationName,
      contactPerson,
      phoneNumber,
      email,
      description
    } = this.state;

    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div className="main-panel" ref="mainPanel">
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">
                  <div className="image">
                    <img src={damirBosnjak} alt="..." />
                  </div>
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      <h6> Smart Survey </h6>
                      Surveys Redifined <br />
                      A Product Of Struts Technology <br />
                      <br />
                      <a href="http://strutstechnology.co.ke" target="_blank">
                        Struts Technology
                      </a>
                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={8} xs={12}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>About Smart Survey</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <p>
                      The Smart Survey system, is a survey tool suitable for
                      carrying out surveys, whereby the surveys are created and
                      ready in minutes for customers to access and give
                      feedback. <br />
                      <br />
                      It allows for USSD Integration, whereby, with the survey
                      questions creatd on the portal, can be available to
                      customers on their mobile phones.
                      <br />
                      <br />
                      <hr />
                      USSD - Unstructured Supplementary Service Data
                      <br />
                      <br />
                      A Product Of Struts Technology
                      <hr />
                      <a href="http://strutstechnology.co.ke" target="_blank">
                        http://strutstechnology.co.ke
                      </a>
                    </p>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default About;
