import React from "react";
import AdminNavBar from "../../shared/AdminNavBar";
import Sidebar from "../../shared/Sidebar";
import Footer from "../../shared/Footer";
import AdminSideBar from "../../shared/AdminSideBar";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

// https://magic.reactjs.net/htmltojsx.htm

class App extends React.Component {
  state = { activeItem: "home" };
  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  render() {
    const { activeItem } = this.state;

    var createReactClass = require("create-react-class");
    var NewComponent = createReactClass({
      render: function() {
        return (
          <div>
            {/* Navigation */}
            <Sidebar />

            {/* Team */}
            <section className="bg-light" id="team">
              {/* Left Menu Bar  */}
              <AdminSideBar />

              <div className="container">
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <h2 className="section-heading text-uppercase">
                      Our Amazing Team
                    </h2>
                    <h3 className="section-subheading text-muted">
                      Lorem ipsum dolor sit amet consectetur.
                    </h3>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="team-member">
                      <img
                        className="mx-auto rounded-circle"
                        src="img/team/1.jpg"
                        alt
                      />
                      <h4>Kay Garland</h4>
                      <p className="text-muted">Lead Designer</p>
                      <ul className="list-inline social-buttons">
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-twitter" />
                          </a>
                        </li>
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-facebook-f" />
                          </a>
                        </li>
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-linkedin-in" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="team-member">
                      <img
                        className="mx-auto rounded-circle"
                        src="img/team/2.jpg"
                        alt
                      />
                      <h4>Larry Parker</h4>
                      <p className="text-muted">Lead Marketer</p>
                      <ul className="list-inline social-buttons">
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-twitter" />
                          </a>
                        </li>
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-facebook-f" />
                          </a>
                        </li>
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-linkedin-in" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="team-member">
                      <img
                        className="mx-auto rounded-circle"
                        src="img/team/3.jpg"
                        alt
                      />
                      <h4>Diana Pertersen</h4>
                      <p className="text-muted">Lead Developer</p>
                      <ul className="list-inline social-buttons">
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-twitter" />
                          </a>
                        </li>
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-facebook-f" />
                          </a>
                        </li>
                        <li className="list-inline-item">
                          <a href="#">
                            <i className="fab fa-linkedin-in" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-8 mx-auto text-center">
                    <p className="large text-muted">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Aut eaque, laboriosam veritatis, quos non quis ad
                      perspiciatis, totam corporis ea, alias ut unde.
                    </p>
                  </div>
                </div>
              </div>
            </section>

            {/* Footer */}
            <Footer />
            {/* Portfolio Modals */}
            {/* Modal 1 */}
            <div
              className="portfolio-modal modal fade"
              id="portfolioModal1"
              tabIndex={-1}
              role="dialog"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="close-modal" data-dismiss="modal">
                    <div className="lr">
                      <div className="rl" />
                    </div>
                  </div>
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-8 mx-auto">
                        <div className="modal-body">
                          {/* Project Details Go Here */}
                          <h2 className="text-uppercase">Project Name</h2>
                          <p className="item-intro text-muted">
                            Lorem ipsum dolor sit amet consectetur.
                          </p>
                          <img
                            className="img-fluid d-block mx-auto"
                            src="img/portfolio/01-full.jpg"
                            alt
                          />
                          <p>
                            Use this area to describe your project. Lorem ipsum
                            dolor sit amet, consectetur adipisicing elit. Est
                            blanditiis dolorem culpa incidunt minus dignissimos
                            deserunt repellat aperiam quasi sunt officia
                            expedita beatae cupiditate, maiores repudiandae,
                            nostrum, reiciendis facere nemo!
                          </p>
                          <ul className="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: Threads</li>
                            <li>Category: Illustration</li>
                          </ul>
                          <button
                            className="btn btn-primary"
                            data-dismiss="modal"
                            type="button"
                          >
                            <i className="fas fa-times" />
                            Close Project
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Modal 2 */}
            <div
              className="portfolio-modal modal fade"
              id="portfolioModal2"
              tabIndex={-1}
              role="dialog"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="close-modal" data-dismiss="modal">
                    <div className="lr">
                      <div className="rl" />
                    </div>
                  </div>
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-8 mx-auto">
                        <div className="modal-body">
                          {/* Project Details Go Here */}
                          <h2 className="text-uppercase">Project Name</h2>
                          <p className="item-intro text-muted">
                            Lorem ipsum dolor sit amet consectetur.
                          </p>
                          <img
                            className="img-fluid d-block mx-auto"
                            src="img/portfolio/02-full.jpg"
                            alt
                          />
                          <p>
                            Use this area to describe your project. Lorem ipsum
                            dolor sit amet, consectetur adipisicing elit. Est
                            blanditiis dolorem culpa incidunt minus dignissimos
                            deserunt repellat aperiam quasi sunt officia
                            expedita beatae cupiditate, maiores repudiandae,
                            nostrum, reiciendis facere nemo!
                          </p>
                          <ul className="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: Explore</li>
                            <li>Category: Graphic Design</li>
                          </ul>
                          <button
                            className="btn btn-primary"
                            data-dismiss="modal"
                            type="button"
                          >
                            <i className="fas fa-times" />
                            Close Project
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Modal 3 */}
            <div
              className="portfolio-modal modal fade"
              id="portfolioModal3"
              tabIndex={-1}
              role="dialog"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="close-modal" data-dismiss="modal">
                    <div className="lr">
                      <div className="rl" />
                    </div>
                  </div>
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-8 mx-auto">
                        <div className="modal-body">
                          {/* Project Details Go Here */}
                          <h2 className="text-uppercase">Project Name</h2>
                          <p className="item-intro text-muted">
                            Lorem ipsum dolor sit amet consectetur.
                          </p>
                          <img
                            className="img-fluid d-block mx-auto"
                            src="img/portfolio/03-full.jpg"
                            alt
                          />
                          <p>
                            Use this area to describe your project. Lorem ipsum
                            dolor sit amet, consectetur adipisicing elit. Est
                            blanditiis dolorem culpa incidunt minus dignissimos
                            deserunt repellat aperiam quasi sunt officia
                            expedita beatae cupiditate, maiores repudiandae,
                            nostrum, reiciendis facere nemo!
                          </p>
                          <ul className="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: Finish</li>
                            <li>Category: Identity</li>
                          </ul>
                          <button
                            className="btn btn-primary"
                            data-dismiss="modal"
                            type="button"
                          >
                            <i className="fas fa-times" />
                            Close Project
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Modal 4 */}
            <div
              className="portfolio-modal modal fade"
              id="portfolioModal4"
              tabIndex={-1}
              role="dialog"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="close-modal" data-dismiss="modal">
                    <div className="lr">
                      <div className="rl" />
                    </div>
                  </div>
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-8 mx-auto">
                        <div className="modal-body">
                          {/* Project Details Go Here */}
                          <h2 className="text-uppercase">Project Name</h2>
                          <p className="item-intro text-muted">
                            Lorem ipsum dolor sit amet consectetur.
                          </p>
                          <img
                            className="img-fluid d-block mx-auto"
                            src="img/portfolio/04-full.jpg"
                            alt
                          />
                          <p>
                            Use this area to describe your project. Lorem ipsum
                            dolor sit amet, consectetur adipisicing elit. Est
                            blanditiis dolorem culpa incidunt minus dignissimos
                            deserunt repellat aperiam quasi sunt officia
                            expedita beatae cupiditate, maiores repudiandae,
                            nostrum, reiciendis facere nemo!
                          </p>
                          <ul className="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: Lines</li>
                            <li>Category: Branding</li>
                          </ul>
                          <button
                            className="btn btn-primary"
                            data-dismiss="modal"
                            type="button"
                          >
                            <i className="fas fa-times" />
                            Close Project
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Modal 5 */}
            <div
              className="portfolio-modal modal fade"
              id="portfolioModal5"
              tabIndex={-1}
              role="dialog"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="close-modal" data-dismiss="modal">
                    <div className="lr">
                      <div className="rl" />
                    </div>
                  </div>
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-8 mx-auto">
                        <div className="modal-body">
                          {/* Project Details Go Here */}
                          <h2 className="text-uppercase">Project Name</h2>
                          <p className="item-intro text-muted">
                            Lorem ipsum dolor sit amet consectetur.
                          </p>
                          <img
                            className="img-fluid d-block mx-auto"
                            src="img/portfolio/05-full.jpg"
                            alt
                          />
                          <p>
                            Use this area to describe your project. Lorem ipsum
                            dolor sit amet, consectetur adipisicing elit. Est
                            blanditiis dolorem culpa incidunt minus dignissimos
                            deserunt repellat aperiam quasi sunt officia
                            expedita beatae cupiditate, maiores repudiandae,
                            nostrum, reiciendis facere nemo!
                          </p>
                          <ul className="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: Southwest</li>
                            <li>Category: Website Design</li>
                          </ul>
                          <button
                            className="btn btn-primary"
                            data-dismiss="modal"
                            type="button"
                          >
                            <i className="fas fa-times" />
                            Close Project
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Modal 6 */}
            <div
              className="portfolio-modal modal fade"
              id="portfolioModal6"
              tabIndex={-1}
              role="dialog"
              aria-hidden="true"
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="close-modal" data-dismiss="modal">
                    <div className="lr">
                      <div className="rl" />
                    </div>
                  </div>
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-8 mx-auto">
                        <div className="modal-body">
                          {/* Project Details Go Here */}
                          <h2 className="text-uppercase">Project Name</h2>
                          <p className="item-intro text-muted">
                            Lorem ipsum dolor sit amet consectetur.
                          </p>
                          <img
                            className="img-fluid d-block mx-auto"
                            src="img/portfolio/06-full.jpg"
                            alt
                          />
                          <p>
                            Use this area to describe your project. Lorem ipsum
                            dolor sit amet, consectetur adipisicing elit. Est
                            blanditiis dolorem culpa incidunt minus dignissimos
                            deserunt repellat aperiam quasi sunt officia
                            expedita beatae cupiditate, maiores repudiandae,
                            nostrum, reiciendis facere nemo!
                          </p>
                          <ul className="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: Window</li>
                            <li>Category: Photography</li>
                          </ul>
                          <button
                            className="btn btn-primary"
                            data-dismiss="modal"
                            type="button"
                          >
                            <i className="fas fa-times" />
                            Close Project
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Bootstrap core JavaScript */}
            {/* Plugin JavaScript */}
            {/* Contact form JavaScript */}
            {/* Custom scripts for this template */}
          </div>
        );
      }
    });

    return (
      <div className="">
        <NewComponent />
      </div>
    );
  }
}

// var ReactDOM = require('react-dom');
// ReactDOM.render(<GroceryItemList/>,app);

export default App;
