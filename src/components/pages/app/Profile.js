import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import CardAuthor from "../../../components/admin/CardElements/CardAuthor.jsx";
import FormInputs from "../../../components/admin/FormInputs/FormInputs.jsx";
// import Button from "../../../components/admin/CustomButton/CustomButton.jsx";
import damirBosnjak from "../../../assets/img/damir-bosnjak.jpg";
import mike from "../../../assets/img/mike.jpg";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";

import dashboardRoutes from "../../../routes/dashboard.jsx";

import {
  logout,
  login,
  isLoggedIn,
  getUserDetails
} from "../../../util/AuthService";

import {
  API_BASE_URL,
  SURVEY_PAYLOAD,
  SUCCESS,
  VERIFICATION_ERROR_CODE,
  VERIFICATION
} from "../../../constants";
import Loader from "../../shared/Loader";
import { Button, Form, Input, Label, TextArea } from "semantic-ui-react";
import axios from "axios";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Profile";

    var loggedIn = getUserDetails();
    console.log(isLoggedIn());
    console.log("loggedIn >>> " + loggedIn);
    console.log(loggedIn);

    this.state = {
      records: [],
      isLoading: false,
      pageOfItems: [],
      organizationID: loggedIn.organization.id,
      organizationName: loggedIn.organization.name,
      contactPerson: loggedIn.organization.contact_person,
      phoneNumber: loggedIn.organization.phone_number,
      email: loggedIn.organization.email,
      description: loggedIn.organization.status_description,
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {
    var loggedIn = getUserDetails();
    console.log("Getting user details ....?");
    console.log(loggedIn);
    var securityToken = "";
    var patientId = 1;
  }

  onSubmit = e => {
    this.setState({ isLoading: true });

    const { organizationID } = this.state;

    var orgUpdatePayload = {
      name: this.state.organizationName,
      contact_person: this.state.contactPerson,
      phone_number: this.state.phoneNumber,
      email: this.state.email
    };
    console.log("Updating Org >>> " + organizationID);
    console.log(orgUpdatePayload);

    axios({
      url: `${API_BASE_URL}/organization/update/` + organizationID,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: orgUpdatePayload
    })
      .then(response => {
        this.setState({ isLoading: false });
        console.log(response.data);
        if (!response.data.error) {
          // alert("Your OTP Is : " + response.data.typeId);
          alert("Organization Updated Successfully.");
          // localStorage.setItem(SURVEY_PAYLOAD, JSON.stringify(response.data));
          this.props.history.push("/profile");
          // localStorage.setItem("Survey_Token", response.data.token);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {
    const {
      organizationName,
      contactPerson,
      phoneNumber,
      email,
      description
    } = this.state;

    console.log("organizationName >>> " + organizationName);

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Update Profile
      </Form.Field>
    );
    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">
                  <div className="image">
                    <img src={damirBosnjak} alt="..." />
                  </div>
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      <h6> {organizationName} </h6>
                      {contactPerson} <br />
                      {phoneNumber} <br />
                      {email} <br />
                      <br />
                      <h6>Current Status </h6>
                      {description}
                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={8} xs={8}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Edit Profile</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Organization Name
                      </Label>
                      <Form.Field className="form-control">
                        {/* <label>Oranization Name</label> */}
                        <input
                          type="text"
                          placeholder="Organization Name*"
                          className="form-control"
                          value={organizationName}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ organizationName: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Contact Person
                      </Label>
                      <Form.Field className="form-control">
                        {/* <label>Oranization Name</label> */}
                        <input
                          type="text"
                          placeholder="Contact Person*"
                          className="form-control"
                          value={contactPerson}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ contactPerson: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Phone Number
                      </Label>
                      <Form.Field className="form-control">
                        {/* <label>Oranization Name</label> */}
                        <input
                          type="text"
                          placeholder="Phone Number*"
                          className="form-control"
                          value={phoneNumber}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ phoneNumber: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Email
                      </Label>
                      <Form.Field className="form-control">
                        {/* <label>Oranization Name</label> */}
                        <input
                          type="text"
                          placeholder="Email*"
                          className="form-control"
                          value={email}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ email: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Row>
                        <div className="">
                          {/* <Button color="primary" round>
                            Update Profile
                          </Button> */}

                          {button}
                          <br />
                        </div>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default Profile;
