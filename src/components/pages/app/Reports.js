import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col
} from "reactstrap";
// react plugin used to create charts
import { Line, Pie } from "react-chartjs-2";
// function that returns a color based on an interval of numbers

import Stats from "../../../components/admin/Stats/Stats.jsx";

import {
  dashboardEmailStatisticsChart,
  dashboardNASDAQChart
} from "../../../variables/charts.jsx";

import PerfectScrollbar from "perfect-scrollbar";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import dashboardRoutes from "../../../routes/dashboard.jsx";

var ps;
// const element = <Sidebar activePage="dashboard" />;

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    // this.props.activePage = "dashboard";
    document.title = "Dashboard";
    this.state = {
      activePage: "dashboard",
      backgroundColor: "black",
      activeColor: "warning"
    };
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.mainPanel);
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentDidUpdate(e) {
    if (e.history.action === "PUSH") {
      this.refs.mainPanel.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
    }
  }
  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };
  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };
  render() {
    return (
      <div
        className=""
        style={{
          overflowX: "hidden",
          overflowY: "hidden",
          msOverflowY: "hidden"
        }}
      >
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />
        <div
          className="main-panel"
          ref="mainPanel"
          style={{
            overflowX: "hidden",
            overflowY: "hidden",
            msOverflowY: "hidden"
          }}
        >
          <Header {...this.props} />
          <div
            className="content"
            style={{
              overflowX: "hidden",
              overflowY: "hidden",
              msOverflowY: "hidden"
            }}
          >
            <Row>
              <Col xs={12} sm={12} md={4}>
                <Card>
                  <CardHeader>
                    <CardTitle>Email Statistics</CardTitle>
                    <p className="card-category">Last Campaign Performance</p>
                  </CardHeader>
                  <CardBody>
                    <Pie
                      data={dashboardEmailStatisticsChart.data}
                      options={dashboardEmailStatisticsChart.options}
                    />
                  </CardBody>
                  <CardFooter>
                    <div className="legend">
                      <i className="fa fa-circle text-primary" /> Opened{" "}
                      <i className="fa fa-circle text-warning" /> Read{" "}
                      <i className="fa fa-circle text-danger" /> Deleted{" "}
                      <i className="fa fa-circle text-gray" /> Unopened
                    </div>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "fas fa-calendar-alt",
                          t: " Number of emails sent"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
              <Col xs={12} sm={12} md={8}>
                <Card className="card-chart">
                  <CardHeader>
                    <CardTitle>NASDAQ: AAPL</CardTitle>
                    <p className="card-category">Line Chart With Points</p>
                  </CardHeader>
                  <CardBody>
                    <Line
                      data={dashboardNASDAQChart.data}
                      options={dashboardNASDAQChart.options}
                      width={400}
                      height={100}
                    />
                  </CardBody>
                  <CardFooter>
                    <div className="chart-legend">
                      <i className="fa fa-circle text-info" /> Tesla Model S{" "}
                      <i className="fa fa-circle text-warning" /> BMW 5 Series
                    </div>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "fas fa-check",
                          t: " Data information certified"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </div>
          {/* <Footer fluid /> */}
        </div>
        {/* <FixedPlugin
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          handleActiveClick={this.handleActiveClick}
          handleBgClick={this.handleBgClick}
        /> */}
      </div>
    );
  }
}

export default Dashboard;
