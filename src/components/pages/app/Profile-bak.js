import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import CardAuthor from "../../../components/admin/CardElements/CardAuthor.jsx";
import FormInputs from "../../../components/admin/FormInputs/FormInputs.jsx";
import Button from "../../../components/admin/CustomButton/CustomButton.jsx";

import damirBosnjak from "../../../assets/img/damir-bosnjak.jpg";
import mike from "../../../assets/img/mike.jpg";

import Header from "../../admin/Header/Header.jsx";
import Footer from "../../admin/Footer/Footer.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";

import dashboardRoutes from "../../../routes/dashboard.jsx";

var ps;

class Profile extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Profile";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };
  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };
  render() {
    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">
                  <div className="image">
                    <img src={damirBosnjak} alt="..." />
                  </div>
                  <CardBody>
                    <CardAuthor
                      avatar={mike}
                      avatarAlt="..."
                      title="Chet Faker"
                      description="@chetfaker"
                    />
                    <p className="description text-center">
                      "I like the way you work it <br />
                      No diggity <br />I wanna bag it up"
                    </p>
                  </CardBody>
                  <CardFooter>
                    <hr />
                    <div className="button-container">
                      <Row>
                        <Col xs={6} sm={6} md={6} lg={3} className="ml-auto">
                          <h5>
                            12
                            <br />
                            <small>Files</small>
                          </h5>
                        </Col>
                        <Col
                          xs={6}
                          sm={6}
                          md={6}
                          lg={4}
                          className="mr-auto ml-auto"
                        >
                          <h5>
                            2GB
                            <br />
                            <small>Used</small>
                          </h5>
                        </Col>
                        <Col lg={3} className="mr-auto">
                          <h5>
                            24,6$
                            <br />
                            <small>Spent</small>
                          </h5>
                        </Col>
                      </Row>
                    </div>
                  </CardFooter>
                </Card>
              </Col>
              <Col md={8} xs={12}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Edit Profile</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <form>
                      <FormInputs
                        ncols={[
                          "col-md-5 pr-1",
                          "col-md-3 px-1",
                          "col-md-4 pl-1"
                        ]}
                        proprieties={[
                          {
                            label: "Company (disabled)",
                            inputProps: {
                              type: "text",
                              disabled: true,
                              defaultValue: "Creative Code Inc."
                            }
                          },
                          {
                            label: "Username",
                            inputProps: {
                              type: "text",
                              defaultValue: "chetfaker"
                            }
                          },
                          {
                            label: "Email address",
                            inputProps: {
                              type: "email",
                              placeholder: "Email"
                            }
                          }
                        ]}
                      />
                      <FormInputs
                        ncols={["col-md-6 pr-1", "col-md-6 pl-1"]}
                        proprieties={[
                          {
                            label: "First Name",
                            inputProps: {
                              type: "text",
                              placeholder: "First Name",
                              defaultValue: "Chet"
                            }
                          },
                          {
                            label: "Last Name",
                            inputProps: {
                              type: "text",
                              placeholder: "Last Name",
                              defaultValue: "Faker"
                            }
                          }
                        ]}
                      />
                      {/* <FormInputs
                        ncols={["col-md-12"]}
                        proprieties={[
                          {
                            label: "Address",
                            inputProps: {
                              type: "text",
                              placeholder: "Home Address",
                              defaultValue: "Melbourne, Australia"
                            }
                          }
                        ]}
                      /> */}
                      <FormInputs
                        ncols={[
                          "col-md-4 pr-1",
                          "col-md-4 px-1",
                          "col-md-4 pl-1"
                        ]}
                        proprieties={[
                          {
                            label: "City",
                            inputProps: {
                              type: "text",
                              defaultValue: "Melbourne",
                              placeholder: "City"
                            }
                          },
                          {
                            label: "Country",
                            inputProps: {
                              type: "text",
                              defaultValue: "Australia",
                              placeholder: "Country"
                            }
                          },
                          {
                            label: "Postal Code",
                            inputProps: {
                              type: "number",
                              placeholder: "ZIP Code"
                            }
                          }
                        ]}
                      />
                      <FormInputs
                        ncols={["col-md-12"]}
                        proprieties={[
                          {
                            label: "About Me",
                            inputProps: {
                              type: "textarea",
                              defaultValue:
                                "Oh so, your weak rhyme You doubt I'll bother, reading into it",
                              placeholder: "Here can be your description"
                            }
                          }
                        ]}
                      />
                      <Row>
                        <div className="update ml-auto mr-auto">
                          <Button color="primary" round>
                            Update Profile
                          </Button>
                        </div>
                      </Row>
                    </form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default Profile;
