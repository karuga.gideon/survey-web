import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col
} from "reactstrap";

import { Line } from "react-chartjs-2";
import Stats from "../../../components/admin/Stats/Stats.jsx";
import { dashboard24HoursPerformanceChart } from "../../../variables/charts.jsx";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";

import dashboardRoutes from "../../../routes/dashboard.jsx";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    // this.props.activePage = "dashboard";
    document.title = "Dashboard";
    this.state = {
      activePage: "dashboard",
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };
  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };
  render() {
    return (
      <div
        className="wrapper"
        style={{
          overflowX: "hidden",
          overflowY: "hidden",
          msOverflowY: "hidden"
        }}
      >
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />
        <div
          className="main-panel"
          ref="mainPanel"
          // style="overflow-y: hidden;"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div
            className="content"
            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
          >
            <Row style={{ overflowX: "hidden", msOverflowY: "hidden" }}>
              <Col
                xs={12}
                sm={6}
                md={6}
                lg={3}
                style={{ overflowX: "hidden", msOverflowY: "hidden" }}
              >
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <Col xs={5} md={4}>
                        <div className="icon-big text-center">
                          <i className="nc-icon nc-globe text-warning" />
                        </div>
                      </Col>
                      <Col xs={7} md={8}>
                        <div className="numbers">
                          <p className="card-category">Total Surveys</p>
                          <CardTitle tag="p">200</CardTitle>
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "fas fa-sync-alt",
                          t: "Update Now"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
              <Col xs={12} sm={6} md={6} lg={3}>
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <Col xs={5} md={4}>
                        <div className="icon-big text-center">
                          <i className="nc-icon nc-money-coins text-success" />
                        </div>
                      </Col>
                      <Col xs={7} md={8}>
                        <div className="numbers">
                          <p className="card-category">Total Questions</p>
                          <CardTitle tag="p"> 1,345</CardTitle>
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "far fa-calendar",
                          t: "Last day"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
              <Col xs={12} sm={6} md={6} lg={3}>
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <Col xs={5} md={4}>
                        <div className="icon-big text-center">
                          <i className="nc-icon nc-vector text-danger" />
                        </div>
                      </Col>
                      <Col xs={7} md={8}>
                        <div className="numbers">
                          <p className="card-category">Avg Resp Time</p>
                          <CardTitle tag="p">23 Sec</CardTitle>
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "far fa-clock",
                          t: "In the last hour"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
              <Col xs={12} sm={6} md={6} lg={3}>
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <Col xs={5} md={4}>
                        <div className="icon-big text-center">
                          <i className="nc-icon nc-favourite-28 text-primary" />
                        </div>
                      </Col>
                      <Col xs={7} md={8}>
                        <div className="numbers">
                          <p className="card-category">Respondents</p>
                          <CardTitle tag="p">+45K</CardTitle>
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "fas fa-sync-alt",
                          t: "Update now"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <Card style={{ height: "80%" }}>
                  <CardHeader style={{ height: "10%", textAlign: "Center" }}>
                    <CardTitle>Users Behavior</CardTitle>
                    <p className="card-category">24 Hours performance</p>
                  </CardHeader>
                  <CardBody style={{ height: "70%" }}>
                    <Line
                      data={dashboard24HoursPerformanceChart.data}
                      options={dashboard24HoursPerformanceChart.options}
                      width={400}
                      height={100}
                    />
                  </CardBody>
                  <CardFooter>
                    <hr />
                    <Stats>
                      {[
                        {
                          i: "fas fa-history",
                          t: " Updated 3 minutes ago"
                        }
                      ]}
                    </Stats>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </div>
          {/* <Footer fluid /> */}
        </div>
        {/* <FixedPlugin
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          handleActiveClick={this.handleActiveClick}
          handleBgClick={this.handleBgClick}
        /> */}
      </div>
    );
  }
}

export default Dashboard;
