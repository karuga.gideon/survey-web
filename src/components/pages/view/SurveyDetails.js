import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import damirBosnjak from "../../../assets/img/damir-bosnjak.jpg";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";

import dashboardRoutes from "../../../routes/dashboard.jsx";

import {
  isLoggedIn,
  getUserDetails,
  getSurveyDetails
} from "../../../util/AuthService";

import { API_BASE_URL, SURVEY_DETAILS } from "../../../constants";
import Loader from "../../shared/Loader";
import { Button, Form, Input, Label, TextArea } from "semantic-ui-react";
import axios from "axios";

class SurveyDetails extends React.Component {
  constructor(props) {
    super(props);
    document.title = "SurveyDetails";

    var surveyID = this.props.match.params.surveyID;
    console.log("surveyID >>> " + surveyID);

    var loggedIn = getUserDetails();
    console.log(isLoggedIn());
    console.log("loggedIn >>> " + loggedIn);
    console.log(loggedIn);

    var surveyDetails = getSurveyDetails();
    console.log("surveyDetails >>> " + surveyDetails);
    console.log(surveyDetails);

    this.state = {
      records: [],
      surveyDetails: {},
      isLoading: false,
      pageOfItems: [],
      surveyID: surveyID,
      organizationID: loggedIn.organization.id,
      organizationName: loggedIn.organization.name,
      contactPerson: loggedIn.organization.contact_person,
      phoneNumber: loggedIn.organization.phone_number,
      email: loggedIn.organization.email,
      description: loggedIn.organization.status_description,
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {
    var loggedIn = getUserDetails();
    console.log("Getting user details ....?");
    console.log(loggedIn);

    this.setState({ isLoading: true });

    const { surveyID } = this.state;
    console.log("Getting Survey Details >>> " + surveyID);

    axios({
      url: `${API_BASE_URL}/get_survey/` + surveyID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log(response.data);
        var surveyDetailsJSON = JSON.stringify(response.data);
        this.setState({ isLoading: false, surveyDetails: response.data });
        console.log(response.data);
        if (!response.data.error) {
          // alert("Your OTP Is : " + response.data.typeId);
          // alert("Survey details pulled Successfully.");
          localStorage.setItem(SURVEY_DETAILS);
          // this.props.history.push("/profile");
          // localStorage.setItem("Survey_Token", response.data.token);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const {
      surveyDetails,
      organizationName,
      contactPerson,
      phoneNumber,
      email,
      description
    } = this.state;

    console.log("Render surveyDetails >>> " + surveyDetails.id);

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
        onClick={this.onSubmit}
      >
        Update Survey
      </Form.Field>
    );
    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col md={4} xs={12}>
                <Card className="card-user">
                  <div className="image">
                    <img src={damirBosnjak} alt="..." />
                  </div>
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      <h6> {surveyDetails.title} </h6>
                      Title : {surveyDetails.title} <br />
                      Target Number : {surveyDetails.target_number} <br />
                      Date Created: {surveyDetails.date_created} <br />
                      <br />
                      <h6>Description </h6>
                      {surveyDetails.description}
                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={8} xs={8}>
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Edit Survey Details</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form>
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Survey Name
                      </Label>
                      <Form.Field className="form-control">
                        {/* <label>Oranization Name</label> */}
                        <input
                          type="text"
                          placeholder="Survey Name*"
                          className="form-control"
                          value={surveyDetails.title}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ title: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Target Number
                      </Label>
                      <Form.Field className="form-control">
                        {/* <label>Oranization Name</label> */}
                        <input
                          type="text"
                          placeholder="Target Number*"
                          className="form-control"
                          value={surveyDetails.target_number}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ contactPerson: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />
                      <Label
                        style={{
                          fontSize: "0.8571em",
                          marginBottom: "5px",
                          color: "#9A9A9A"
                        }}
                      >
                        Description
                      </Label>
                      <Form.Field className="form-control">
                        {/* <label>Oranization Name</label> */}
                        <TextArea
                          type="text"
                          placeholder="Short Description"
                          className="form-control"
                          value={surveyDetails.description}
                          style={{
                            textDecoration: "none",
                            padding: "0px",
                            border: "0px"
                          }}
                          onChange={e =>
                            this.setState({ description: e.target.value })
                          }
                        />
                      </Form.Field>
                      <br />

                      <Row>
                        <div className="">
                          {/* <Button color="primary" round>
                            Update SurveyDetails
                          </Button> */}

                          {button}
                          <br />
                        </div>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
      </div>
    );
  }
}

export default SurveyDetails;
