import React from "react";
import Footer from "../shared/Footer";
import { Link } from "react-router-dom";
import { Grid, Header, Button, Form, Input } from "semantic-ui-react";
import axios from "axios";
import {
  API_BASE_URL,
  SURVEY_PAYLOAD,
  SUCCESS,
  VERIFICATION_ERROR_CODE,
  VERIFICATION
} from "../../constants";
import Loader from "../shared/Loader";

// https://magic.reactjs.net/htmltojsx.htm

class App extends React.Component {
  constructor(props) {
    super(props);

    document.title = "Sign In";

    this.state = {
      email: "",
      password: "",
      isLoading: false
    };
  }

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  onSubmit = e => {
    this.setState({ isLoading: true });

    var loginPayload = {
      email: this.state.email,
      password: this.state.password
    };
    console.log("Org login...");
    console.log(loginPayload);

    axios({
      url: `${API_BASE_URL}/organization/login`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: loginPayload
    })
      .then(response => {
        this.setState({ isLoading: false });
        console.log(response.data);
        if (!response.data.error) {
          // alert("Your OTP Is : " + response.data.typeId);
          localStorage.setItem(SURVEY_PAYLOAD, JSON.stringify(response.data));
          this.props.history.push("/dashboard");
          localStorage.setItem("Survey_Token", response.data.token);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {
    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#fed136" }}
        onClick={this.onSubmit}
      >
        Sign In
      </Form.Field>
    );

    return (
      <div>
        {/* Navigation */}
        {/* <Navbar /> */}

        {/* Contact */}
        <section id="contact">
          <div className="container">
            <div className="row" style={{ marginTop: "-120px" }}>
              <div className="col-lg-12 text-center">
                <div id="mainNav">
                  <Link className="navbar-brand " to="/">
                    Smart Survey
                  </Link>
                </div>
                <h4 className="section-heading text-uppercase">Sign In</h4>
                {/* <h3 className="section-subheading text-muted">
                      Kindly fill in all the details.
                    </h3> */}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3" />
              <div className="col-lg-6">
                <Form>
                  <Form.Field className="form-control">
                    {/* <label>Email</label> */}
                    <input
                      type="email"
                      placeholder="email"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e => this.setState({ email: e.target.value })}
                    />
                  </Form.Field>
                  <br />

                  <Form.Field className="form-control">
                    {/* <label>Password</label> */}
                    <input
                      type="password"
                      placeholder="password"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                    />
                  </Form.Field>
                  <br />

                  <div className="col-lg-12 text-center">
                    <br />
                    {button}
                  </div>

                  <Grid>
                    <Grid.Column mobile={6} tablet={8} computer={16}>
                      <Grid.Column
                        mobile={6}
                        tablet={8}
                        computer={16}
                        floated="right"
                      />
                    </Grid.Column>
                  </Grid>
                </Form>

                <br />
                <h3 className="section-subheading text-muted text-center">
                  Not Registered? <Link to="/signup">Sign Up</Link>
                </h3>
              </div>
            </div>
          </div>
        </section>
        {/* Footer */}
        <Footer />
        {/* Portfolio Modals */}

        {/* Bootstrap core JavaScript */}
        {/* Plugin JavaScript */}
        {/* Contact form JavaScript */}
        {/* Custom scripts for this template */}
      </div>
    );
  }
}

// var ReactDOM = require('react-dom');
// ReactDOM.render(<GroceryItemList/>,app);

export default App;
