import React from "react";
import { NavLink, Link } from "react-router-dom";
import { Nav } from "reactstrap";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";

// import logo from "../../../logo.svg";

var ps;

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    // console.log("Active routeName >>> " + routeName);
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }

  render() {
    // console.log("props.activeClassName >>> " + this.props.location.pathname);

    return (
      <div
        className="sidebar"
        data-color={this.props.bgColor}
        data-active-color={this.props.activeColor}
      >
        <div className="logo">
          <Link
            className="navbar-brand survey-system-brand"
            style={{
              color: "#fed136",
              display: "block",
              fontFamily: `"Kaushan Script", -apple-system, BlinkMacSystemFont, "Segoe UI",
               Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji",
               "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"`,
              fontSize: "27.0px",
              lineHeight: "26px",
              fontWeight: "400",
              padding: "18px"
            }}
            to="/dashboard"
          >
            Smart Survey
          </Link>
        </div>
        <div
          className="sidebar-wrapper"
          ref="sidebar"
          style={{ overflowX: "hidden", overflowY: "hidden" }}
        >
          <Nav>
            <li className={this.activeRoute("dashboard")}>
              <NavLink
                to="/dashboard"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-bank" />
                <p>Dashboard</p>
              </NavLink>
            </li>
            <li
              className={
                this.activeRoute("surveys") ||
                this.activeRoute("create-survey") ||
                this.activeRoute("survey-details") ||
                this.activeRoute("survey-questions")
              }
            >
              <NavLink
                to="/surveys"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-bullet-list-67" />
                <p>Surveys</p>
              </NavLink>
            </li>
            {/* <li className={this.activeRoute("topup")}>
              <NavLink
                to="/topup"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-credit-card" />
                <p>Top Up</p>
              </NavLink>
            </li> */}
            <li className={this.activeRoute("reports")}>
              <NavLink
                to="/reports"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-tile-56" />
                <p>Reports</p>
              </NavLink>
            </li>
            <li className={this.activeRoute("profile")}>
              <NavLink
                to="/profile"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-badge" />
                <p>Profile</p>
              </NavLink>
            </li>
            <li className={this.activeRoute("about")}>
              <NavLink
                to="/about"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-diamond" />
                <p>About</p>
              </NavLink>
            </li>
            {/* {this.props.routes.map((prop, key) => {
              if (prop.redirect) return null;
              return (
                <li
                  className={
                    this.activeRoute(prop.path) +
                    (prop.pro ? " active-pro" : "")
                  }
                  key={key}
                >
                  <NavLink
                    to={prop.path}
                    className="nav-link"
                    activeClassName="active"
                  >
                    <i className={prop.icon} />
                    <p>{prop.name}</p>
                  </NavLink>
                </li>
              );
            })} */}
          </Nav>
        </div>
      </div>
    );
  }
}

export default Sidebar;
