import React, { Component } from "react";
import { Link } from "react-router-dom";
import savepayLogo from "../../logo.svg";
import {
  Menu,
  Responsive,
  Visibility,
  Dropdown,
  Container
} from "semantic-ui-react";
import "../../AdminApp.css";

export default class AdminSideBar extends Component {
  state = { activeItem: "account" };

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;

    return (
      <div>
        <div class="sidenav">
          <div className="admin-logo" href="#">
            &nbsp; Smart Survey
          </div>
          <br />
          <a href="#services">Dashboard</a>
          <a href="#clients">Surveys</a>
          <a href="#contact">Reports</a>
        </div>
      </div>
    );
  }
}
